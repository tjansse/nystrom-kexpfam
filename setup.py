from setuptools import setup, find_packages


setup(name='nystrom-kexpfam',
      version='0.1',
      description='Experimental codes for AISTATS 2018 paper "Efficient and principled score estimation with Nyström kernel exponential families" by Dougal Sutherland, Heiko Strathmann, Michael Arbel, and Arthur Gretton, https://arxiv.org/abs/1705.08360.',
      url='https://github.com/karlnapf/nystrom-kexpfam',
      author='Heiko Strathmann',
      author_email='heiko.strathmann@gmail.com',
      license='BSD3',
      packages=find_packages('.'),
zip_safe=False)
